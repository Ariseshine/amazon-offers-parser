require_relative '../lib/seller.rb'
RSpec.describe Seller do

	context '.parse_seller_name' do
		it "should get seller name" do
			seller_name = Seller.parse_seller_name('<body><div class="a-column a-span2 olpSellerColumn" role="gridcell"><h3 class="a-spacing-none olpSellerName"><span class="a-size-medium a-text-bold"> <a href="/gp/aag/main/ref=olp_merch_name_2?ie=UTF8&amp;asin=0761455299&amp;isAmazonFulfilled=0&amp;seller=A19A29L7PVBL05">D M Books Store</a> </span></h3><p class="a-spacing-small"> <i class="a-icon a-icon-star a-star-4-5"><span class="a-icon-alt">4.5 out of 5 stars</span></i> <a href="/gp/aag/main/ref=olp_merch_rating_2?ie=UTF8&amp;asin=0761455299&amp;isAmazonFulfilled=0&amp;seller=A19A29L7PVBL05"><b>92% positive</b></a> over the past 12 months. (409 total ratings) <br> </p></div></body>')
			expect(seller_name).to eq('D M Books Store')
		end
	end

	context '.parse_seller_id' do
		it "should get seller id" do
			seller_id = Seller.parse_seller_id('<body><div class="a-column a-span2 olpSellerColumn" role="gridcell"><h3 class="a-spacing-none olpSellerName"><span class="a-size-medium a-text-bold"> <a href="/gp/aag/main/ref=olp_merch_name_2?ie=UTF8&amp;asin=0761455299&amp;isAmazonFulfilled=0&amp;seller=A19A29L7PVBL05">D M Books Store</a> </span></h3><p class="a-spacing-small"> <i class="a-icon a-icon-star a-star-4-5"><span class="a-icon-alt">4.5 out of 5 stars</span></i> <a href="/gp/aag/main/ref=olp_merch_rating_2?ie=UTF8&amp;asin=0761455299&amp;isAmazonFulfilled=0&amp;seller=A19A29L7PVBL05"><b>92% positive</b></a> over the past 12 months. (409 total ratings) <br> </p></div></body>')
			expect(seller_id).to eq('A19A29L7PVBL05')
		end
	end

	context '.parse_feedback' do
		it "should get feedback" do
			feedback = Seller.parse_feedback('<body><div class="a-column a-span2 olpSellerColumn" role="gridcell"><h3 class="a-spacing-none olpSellerName"><span class="a-size-medium a-text-bold"> <a href="/gp/aag/main/ref=olp_merch_name_2?ie=UTF8&amp;asin=0761455299&amp;isAmazonFulfilled=0&amp;seller=A19A29L7PVBL05">D M Books Store</a> </span></h3><p class="a-spacing-small"> <i class="a-icon a-icon-star a-star-4-5"><span class="a-icon-alt">4.5 out of 5 stars</span></i> <a href="/gp/aag/main/ref=olp_merch_rating_2?ie=UTF8&amp;asin=0761455299&amp;isAmazonFulfilled=0&amp;seller=A19A29L7PVBL05"><b>92% positive</b></a> over the past 12 months. (409 total ratings) <br> </p></div></body>')
			expect(feedback).to eq(92)
		end
	end

	context '.parse_ratings' do
		it "should get ratings" do
			ratings = Seller.parse_ratings('<body><div class="a-column a-span2 olpSellerColumn" role="gridcell"><h3 class="a-spacing-none olpSellerName"><span class="a-size-medium a-text-bold"> <a href="/gp/aag/main/ref=olp_merch_name_2?ie=UTF8&amp;asin=0761455299&amp;isAmazonFulfilled=0&amp;seller=A19A29L7PVBL05">D M Books Store</a> </span></h3><p class="a-spacing-small"> <i class="a-icon a-icon-star a-star-4-5"><span class="a-icon-alt">4.5 out of 5 stars</span></i> <a href="/gp/aag/main/ref=olp_merch_rating_2?ie=UTF8&amp;asin=0761455299&amp;isAmazonFulfilled=0&amp;seller=A19A29L7PVBL05"><b>92% positive</b></a> over the past 12 months. (409 total ratings) <br> </p></div></body>')
			expect(ratings).to eq(409)
		end
	end

	context '.parse_seller_name' do
		it "should get seller name" do
			seller_name = Seller.parse_seller_name('<div class="a-column a-span2 olpSellerColumn" role="gridcell"><h3 class="a-spacing-none olpSellerName"><span class="a-size-medium a-text-bold"><a href="/gp/aag/main/ref=olp_merch_name_7?ie=UTF8&amp;asin=0545261244&amp;isAmazonFulfilled=0&amp;seller=AYXDDSK9W0RO9">Kathysshop</a></span></h3><p class="a-spacing-small"><b class="olpJustLaunched">Just Launched</b>(<span class="olpSellerProfile"><a href="/gp/aag/main/ref=olp_merch_new_seller_7?ie=UTF8&amp;asin=0545261244&amp;isAmazonFulfilled=0&amp;seller=AYXDDSK9W0RO9">Seller Profile</a></span>)</p></div>')
			expect(seller_name).to eq('Kathysshop')
		end
	end

	context '.parse_seller_id' do
		it "should get seller id" do
			seller_id = Seller.parse_seller_id('<div class="a-column a-span2 olpSellerColumn" role="gridcell"><h3 class="a-spacing-none olpSellerName"><span class="a-size-medium a-text-bold"><a href="/gp/aag/main/ref=olp_merch_name_7?ie=UTF8&amp;asin=0545261244&amp;isAmazonFulfilled=0&amp;seller=AYXDDSK9W0RO9">Kathysshop</a></span></h3><p class="a-spacing-small"><b class="olpJustLaunched">Just Launched</b>(<span class="olpSellerProfile"><a href="/gp/aag/main/ref=olp_merch_new_seller_7?ie=UTF8&amp;asin=0545261244&amp;isAmazonFulfilled=0&amp;seller=AYXDDSK9W0RO9">Seller Profile</a></span>)</p></div>')
			expect(seller_id).to eq('AYXDDSK9W0RO9')
		end
	end

	context '.parse_feedback' do
		it "should get seller feedback" do
			feedback = Seller.parse_feedback('<div class="a-column a-span2 olpSellerColumn" role="gridcell"><h3 class="a-spacing-none olpSellerName"><span class="a-size-medium a-text-bold"><a href="/gp/aag/main/ref=olp_merch_name_7?ie=UTF8&amp;asin=0545261244&amp;isAmazonFulfilled=0&amp;seller=AYXDDSK9W0RO9">Kathysshop</a></span></h3><p class="a-spacing-small"><b class="olpJustLaunched">Just Launched</b>(<span class="olpSellerProfile"><a href="/gp/aag/main/ref=olp_merch_new_seller_7?ie=UTF8&amp;asin=0545261244&amp;isAmazonFulfilled=0&amp;seller=AYXDDSK9W0RO9">Seller Profile</a></span>)</p></div>')
			expect(feedback).to eq(nil)
		end
	end

	context '.parse_ratings' do
		it "should get seller ratings" do
			ratings = Seller.parse_ratings('<div class="a-column a-span2 olpSellerColumn" role="gridcell"><h3 class="a-spacing-none olpSellerName"><span class="a-size-medium a-text-bold"><a href="/gp/aag/main/ref=olp_merch_name_7?ie=UTF8&amp;asin=0545261244&amp;isAmazonFulfilled=0&amp;seller=AYXDDSK9W0RO9">Kathysshop</a></span></h3><p class="a-spacing-small"><b class="olpJustLaunched">Just Launched</b>(<span class="olpSellerProfile"><a href="/gp/aag/main/ref=olp_merch_new_seller_7?ie=UTF8&amp;asin=0545261244&amp;isAmazonFulfilled=0&amp;seller=AYXDDSK9W0RO9">Seller Profile</a></span>)</p></div>')
			expect(ratings).to eq(nil)
		end
	end

	context '.parse_seller_name' do
		it "should get seller name" do
			seller_name = Seller.parse_seller_name('<div class="a-column a-span2 olpSellerColumn" role="gridcell"><h3 class="a-spacing-none olpSellerName"><img alt="Amazon.com" src="https://images-na.ssl-images-amazon.com/images/I/01dXM-J1oeL.gif"></h3></div>')
			expect(seller_name).to eq('Amazon')
		end
	end

	context '.parse_seller_id' do
		it "should get seller id" do
			seller_id = Seller.parse_seller_id('<div class="a-column a-span2 olpSellerColumn" role="gridcell"><h3 class="a-spacing-none olpSellerName"><img alt="Amazon.com" src="https://images-na.ssl-images-amazon.com/images/I/01dXM-J1oeL.gif"></h3></div>')
			expect(seller_id).to eq(nil)
		end
	end

	context '.parse_feedback' do
		it "should get seller feedback" do
			feedback = Seller.parse_feedback('<div class="a-column a-span2 olpSellerColumn" role="gridcell"><h3 class="a-spacing-none olpSellerName"><img alt="Amazon.com" src="https://images-na.ssl-images-amazon.com/images/I/01dXM-J1oeL.gif"></h3></div>')
			expect(feedback).to eq(nil)
		end
	end

	context '.parse_ratings' do
		it "should get seller ratings" do
			ratings = Seller.parse_ratings('<div class="a-column a-span2 olpSellerColumn" role="gridcell"><h3 class="a-spacing-none olpSellerName"><img alt="Amazon.com" src="https://images-na.ssl-images-amazon.com/images/I/01dXM-J1oeL.gif"></h3></div>')
			expect(ratings).to eq(nil)
		end
	end

		
end