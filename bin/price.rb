require 'nokogiri'

class Price

	attr_reader :sale_price, :parse_shipping_fee

	def initilaize(price_html)
		@price_html = price_html
	end

	def parse(price_html)
		@price_block = Nokogiri::HTML(price_html)
		sale_price = doc.at_css('.')
		shipping_fee = doc.at_css('.')
		@sale_price = Price.parse_sale_price()
		@parse_shipping_fee = Price.parse_shipping_fee(shipping_fee)
	end

	def self.parse_sale_price(sale_price_html)
		if (sale_price.is_a?(string))
			doc = Nokogiri::HTML(sale_price)
		else
			doc = sale_price
		end
		doc.text.gsub(/[^0-9\.]/, '').to_f
	end

	def self.parse_shipping_fee(shipping_fee_html)
		if (shipping_fee.is_a?(string))
			doc = Nokogiri::HTML(shipping_fee_html)
		else
			doc = shipping_fee
		end
		doc = Nokogiri::HTML(shipping_fee_html)
		free_shipping = doc.at_css('b')
		if (free_shipping.text)
			0
		else
			shipping_fee_doc = doc.at_css()
			doc.text.gsub(/[^0-9\.]/, '').to_f

	end

end

