require 'nokogiri'

class Seller

	def self.parse_seller_name(seller_name_html)
		doc = Nokogiri::HTML(seller_name_html)
		seller_name = doc.at_css('a')
		if seller_name == nil
			seller_name = 'Amazon'
		else
			seller_name = seller_name.text
		end
	end

	def self.parse_seller_id(seller_id_html)
		doc = Nokogiri::HTML(seller_id_html)
		seller_id = doc.at_css('a')
		if seller_id != nil
			seller_id = seller_id.attributes.to_s.split('seller=')[-1].split('"')[0]
		end
	end

	def self.parse_feedback(feedback_html)
		doc = Nokogiri::HTML(feedback_html)
		feedback = doc.xpath('/html/body/div/p/a/b').text
		if feedback == ''
			feedback = nil
		else
			feedback = feedback.gsub(/\D/, '').to_f
		end
	end

	def self.parse_ratings(ratings_html)
		doc = Nokogiri::HTML(ratings_html)
		if doc.text == ''
			ratings = nil
		elsif doc.text.include? 'Just Launched'
			ratings = nil
		else
			ratings = doc.xpath('/html/body/div/p').text.split('(')[-1].gsub(/\D/, '').to_f
		end
	end

end

